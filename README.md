
# Admin Panel Template Boiler Plate

If you want to start the project first you have to install the packages by running below command

## Install Packages
npm install

## Folder Structure

`NOTE:`  Make sure to strictly follow the define pattern of the boiler plate in your project. Must go through the structure of the project before starting the project.

1. END POINTS SHOULD BE DEFINED IN VARIABLES' FILE IN CONFIG FOLDER `./src/config/variables/index.js`
2. ALL REUSABLE FUNCTIONS SHOULD BE DEFINED IN UTIL FOLDER `./src/config/util/index.js`
3. ALL FONTS & COLORS SHOULD BE PREDEFINED IN THE RESPECTIVE FOLDERS `./src/config`
4. ALL NAVIGATORS SHOULD BE DEFINED IN NAVIGATION CONFIG FOLDER `./src/navigationConfig`
5. ALL ACTIONS, REDUCERS, MIDDLEWARES SAGAS & CONSTANTS SHOULD BE DEFINED IN RESPECTIVE FOLDERS `./src/store`
6. ALL REUSABLE COMPONENTS SUCH AS BUTTONS, TEXTFIELD etc. SHOULD BE DEFINED IN THE COMPONENTS FOLDER WITH PREDEFINED PROPS WITH DEFAULT VALUES. ALL COMPONENTS SHOULD BE WRAPPED IN REACT MEMO `React.memo(Component)` IN ORDER TO PREVENT RERENDERING (refer to pre-created components) `./src/components`