const Colors = {
    PlaceHolder: (opacity = "0.5") => `rgba(235, 235, 235, ${opacity})`,
    BlackOpacity: (opacity = "0.5") => `rgba(0, 0, 0, ${opacity})`,
    WhiteOpacity: (opacity = "0.5") => `rgba(255, 255, 255, ${opacity})`,
    Black:"#000000",
    Transparent: "transparent",
    Primary: "#d94826",
    Secondary: "#000000",
    DarkGray: "#1E1E22",
    White: "#ffffff",
    Text: "#ffffff",
    Danger: "#FF494C",
}

export default Colors;