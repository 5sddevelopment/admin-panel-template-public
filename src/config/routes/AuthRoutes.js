import React, { useEffect } from "react";
import { Routes, Route, Navigate, useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import { SignIn, Dashboard, Users } from "../../containers";
import DashboardLayout from "../../components/Layout/dashboardLayout.jsx";
import AppRoutes from "./AppRoutes";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";


const AuthRoutes = () => {


  const user = useSelector((state) => state.AppReducer?.user)
  // const navigate = useNavigate();

  // console.log("user", window.location.pathname);
  // if (window.location.pathname === "/login" && user?.email) {
  //   navigate("/dashboard")
  // }
  // if (window.location.pathname === "/dashboard" && user?.email) {
  //   navigate("/dashboard")
  // }

  return (
    <>
      {!user?.email ? (
        <Routes>
          <>
            <Route path="/login" element={<SignIn />} />
            {/* <Route path="/forget-password" element={ForgetPassword} />
            <Route path="/verify-code" element={VerifyCode} />
            <Route path="/reset-password" element={ResetPassword} /> */}
            <Route path="*" element={<Navigate to="/login" />} />
          </>
        </Routes>
      ) : (
        <DashboardLayout expandible={true}>
          <AppRoutes />
        </DashboardLayout>
      )}
    </>
  )
}

export default AuthRoutes
