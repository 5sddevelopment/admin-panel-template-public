import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { connect } from "react-redux";
import {
  Dashboard,
  Users,
  BasicTable,
  TabsTable,
  Form,
  TabsForm,
  Messages
} from "../../containers";

const AppRoutes = () => {
  return (
    <Routes>
      <Route  path="/dashboard" element={<Dashboard />} />
      <Route exact path="/users" element={<Users />} />
      <Route exact path="/basictable" element={<BasicTable />} />
      <Route exact path="/tabstable" element={<TabsTable />} />
      <Route exact path="/form" element={<Form />} />
      <Route exact path="/messages" element={<Messages />} />
      <Route exact path="/tabsform" element={<TabsForm />} />
      <Route  path="*" element={<Navigate to="/dashboard" />} />
    </Routes>
  )
}

export default AppRoutes