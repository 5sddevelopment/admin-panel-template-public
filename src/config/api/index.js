import Axios from "axios";
// import AsyncStorage from "@react-native-community/async-storage";
import { AppAction } from "../../store/actions";
import Store from "../../store";
import variables from "../variables";

const { baseUrl } = variables

// Axios.interceptors.response.use((response) => {
//     return response
// }, ({ response }) => {
//     if (response.status == 401) {
//         try {
//             AsyncStorage.removeItem("user").then(() => {
//                 Store.dispatch(AppAction.SignoutSuccess());
//                 NavigationService.reset_0("Signin")
//             })
//             console.log('%c{Error 401}', "color: red", response)
//         }
//         catch (err) {
//             console.log(`%c${err.name}`, "color: red", err?.message)
//         }
//     }
//     return response
// })

export default class ApiCaller {

    static Get = (endPoint = "", headers = {}) => {
        return Axios.get(`${baseUrl}${endPoint}`, {
            headers
        }).then((res) => res).catch((err) => err.response)
    }

    static Post = (endPoint = "", body = {}, headers = {}) => {
        return Axios.post(`${baseUrl}${endPoint}`, body, {
            headers
        }).then((res) => res).catch((err) => err.response)
    }
}