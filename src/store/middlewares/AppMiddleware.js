import { AppAction } from '../actions';
import { ApiCaller } from '../../config';
import { put, delay } from 'redux-saga/effects';
import { Utils } from "../../config"

export default class AppMiddleware {

    static *SignIn(params) {
        const { payload, cb } = params;
        try {
            // let res = yield ApiCaller.Post(
            //     "signin",
            //     payload
            // )
            // if (res.status == 200) {
            //     yield put(AppAction.SignInSuccess(res.data))
            //     console.log('%cSignIn Response', "color: green", ' => ', res)
            // }
            // else {
            //     yield put(AppAction.SignInFailure())
            //     console.log('%cSignIn Response', "color: red", ' => ', res)
            // }
            yield delay(2000)
            yield localStorage.setItem("user", JSON.stringify(payload))
            yield put(AppAction.SignInSuccess(payload))
            Utils.showMessage('success', "success")
            if (cb) {
                cb() 
            }

        }
        catch (err) {
            yield put(AppAction.SignInFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }

    static *Logout({ payload }) {
        try {
            // let res = yield ApiCaller.Post(
            //     "logout",
            //     payload
            // )
            // if (res.status == 200) {
            //     yield put(AppAction.LogoutSuccess())
            //     console.log('%Logout Response', "color: green", ' => ', res)
            // } else {
            //     yield put(AppAction.LogoutFailure())
            //     console.log('%Logout Response', "color: red", ' => ', res)
            // }
            yield delay(1000)
            localStorage.removeItem("user")
            yield put(AppAction.SignInSuccess(payload))
            Utils.showMessage('success', "Logout")


        }
        catch (err) {
            yield put(AppAction.LogoutFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }

    static *AddPost({ payload }) {
        try {
            let res = yield ApiCaller.Post(
                "add_posts",
                payload
            )
            if (res.status == 200) {
                console.log('%cAddPost Response', "color: green", ' => ', res)
                yield put(AppAction.AddPostSuccess(res.data))
            } else {
                yield put(AppAction.AddPostFailure())
                console.log('%cAddPost Response', "color: red", ' => ', res)
            }
        }
        catch (err) {
            yield put(AppAction.AddPostFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }

    static *GetPosts() {
        try {
            let res = yield ApiCaller.Get("posts")
            if (res.status == 200) {
                console.log('%cGetPosts Response', "color: green", ' => ', res)
                yield put(AppAction.GetPostsSuccess(res.data))
            } else {
                console.log('%cGetPosts Response', "color: red", ' => ', res)
                yield put(AppAction.GetPostsFailure())
            }
        }
        catch (err) {
            yield put(AppAction.GetPostsFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }

}