import { Switch } from "antd";
import React from "react";
import Text from "../Text";


const Basic = ({
  defaultChecked,
  checked,
  disabled,
  size,
  onChange,
  containerStyle,
  ...props
}) => {
  return (
    <div style={containerStyle}>
      {props.label && (
        <Text.Basic text={props.label} textAlign={"left"} fontSize={"16px"} />
      )}
      <Switch
        defaultChecked={defaultChecked}
        onChange={onChange}
        checked={checked}
        disabled={disabled}
        size={size}
        {...props}
      />
    </div>
  );
};

export default Basic;
