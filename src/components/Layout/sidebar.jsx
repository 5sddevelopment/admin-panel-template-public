import {
  LayoutFilled,
  UserOutlined,
  TableOutlined,
  TabletOutlined,
  MailOutlined,
  StepForwardFilled
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { Menu } from "antd";
import React from "react";

import "./sidebar.css";
import Logo from "../../assets/images/logo.png";

const SideBar = ({ collapsed }) => {
  const renderMainNavOptions = () => {
    return (
      <>
        <Menu.Item className="menu-item" key="2" icon={<LayoutFilled />}>
          <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item className="menu-item" key="3" icon={<UserOutlined />}>
          <Link to="/users">Users</Link>
        </Menu.Item>
        <Menu.Item className="menu-item" key="4" icon={<TableOutlined />}>
          <Link to="/basictable">Basic Table</Link>
        </Menu.Item>
        <Menu.Item className="menu-item" key="5" icon={<TabletOutlined />}>
          <Link to="/tabstable">Tabs Table</Link>
        </Menu.Item>
        <Menu.Item className="menu-item" key="6" icon={<TabletOutlined />}>
          <Link to="/form">Use Form</Link>
        </Menu.Item>
        <Menu.Item className="menu-item" key="7" icon={<StepForwardFilled />}>
          <Link to="/tabsform">Steps Form</Link>
        </Menu.Item>
        <Menu.Item className="menu-item" key="8" icon={<MailOutlined />}>
          <Link to="/messages">Messages</Link>
        </Menu.Item>
      </>
    );
  };

  return (
    <div className={`sidebar ${collapsed && "sidebar-closed"}`}>
      <div className="logo">
        <img src={Logo} />
        {!collapsed && <span>Admin Panel</span>}
      </div>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={["1"]}
        className="sidebar-menu dashboard-menu"
      >
        {renderMainNavOptions()}
      </Menu>
    </div>
  );
};
export default SideBar;
