import React from "react";
import { Col, Modal, Row } from "antd";
import Button from "../Button";
import BasicModalBody from "./BasicModalBody";
import ModalCommentBody from "./CommentModalBody";

const BasicModal = ({
  modalType,
  title,
  className,
  onCancel,
  closeable,
  text,
  isModalOpen,
  leftButtonText,
  onClickLeft,
  rightButtonText,
  onClickRight,
  btnLoadingLeft,
  loadingLeft,
  btnLoadingRight,
  loadingRight,
  isFooter,
  isCommentModal,
}) => {
  console.log("description: ", text);

  return (
    <Modal
      className={`basicModal ${className}`}
      title={title}
      open={isModalOpen}
      onCancel={onCancel}
      closable={closeable}
      footer={[
        isFooter?.showFooter ? (
          <Row>
            <Col span={12}>
              <Button.Basic
                className="footer-action footer-btn-1"
                text={isFooter?.leftButtonText}
                onClick={isFooter?.onClickLeft}
                disabled={isFooter?.btnLoadingLeft}
                isShowLoader={isFooter?.loadingLeft}
                size="large"
              />
            </Col>
            <Col span={12}>
              <Button.Basic
                className="footer-action footer-btn-2"
                type="primary"
                size={"large"}
                text={isFooter?.rightButtonText}
                onClick={isFooter?.onClickRight}
                disabled={isFooter?.btnLoadingRight}
                isShowLoader={isFooter?.loadingRight}
              />
            </Col>
          </Row>
        ) : null,
      ]}
    >
      {modalType === "basicModal" ? (
        <BasicModalBody description={text} />
      ) : modalType == "commentModal" ? (
        <ModalCommentBody isCommentModal={isCommentModal} />
      ) : (
        ""
      )}
    </Modal>
  );
};

export default BasicModal;
