import Basic from "./Basic.jsx";
import Confirmation from "./Confirmation.jsx";
import BasicModal from "./BasicModal.jsx";

export default {
  Basic,
  Confirmation,
  BasicModal,
};
