import React from "react";
import { Row, Col } from "antd";

const BasicModalBody = ({ description }) => {
  return (
    <Row>
      <Col span={24}>
        <p className="descriptionPara">{description}</p>
      </Col>
    </Row>
  );
};

export default BasicModalBody;
