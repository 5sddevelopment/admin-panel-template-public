import { Card, Col, Row } from 'antd';
const App = ({

    content = "Card",
    bordered = false

}) => (

    <Card bordered={bordered} >
        <p style={{ textAlign: "center" }}>{content}</p>
    </Card>

);
export default App;