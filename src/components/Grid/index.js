import React from "react";
import { Row, Col, Table, Empty, Button } from "antd";
import { RightOutlined,LeftOutlined } from "@ant-design/icons";

// import LoaderGif from "../../assets/images/ie_loader.gif";

const TableWrapper = ({
    tableData,
    tableColumns,
    onHandleChange,
    previousPage,
    currentPage,
    metaData,
    isLoader,
    allowedAccountLoading,
    fromOrder,
    sorter
}) => {
    let LoaderGif = ""
    return (
        <Row gutter={[0, 20]}>
            <Col justify="start" xs={24} sm={24} md={24} lg={24} xl={24}>
                {tableData?.length ? (
                    <Table

                        columns={tableColumns}
                        dataSource={tableData}
                        onChange={onHandleChange}
                        pagination={false}
                        className="ant-table-tbod"
                    />
                ) : (
                    <>
                        {isLoader && <img src={LoaderGif} height={"60"} width={"60"} />}
                        {!isLoader && <Empty description={"Nothing to show right now."} />}
                    </>
                )}

            </Col>
            {Boolean(tableData?.length) && (
                <Col xs={24} sm={24} md={23} lg={23} xl={23}>
                    <Row
                        gutter={[5, 0]}
                        xs={24}
                        sm={24}
                        md={23}
                        lg={23}
                        xl={23}
                        justify={fromOrder ? "start" : "end"}
                    >
                        <Col className="tablePagination">
                            {`Displaying ${metaData?.currentCount} of ${metaData?.totalCount}`}
                            <Button
                                type="link"
                                icon={<LeftOutlined color="#686868" size={15} />}
                                onClick={previousPage}
                                disabled={metaData?.page === 1}
                            />
                            {/* For  Showing Page-No */}
                            {metaData?.page}
                            <Button
                                type="link"
                                icon={<RightOutlined color="#686868" size={15} />}
                                onClick={currentPage}
                                disabled={metaData?.total_pages === metaData?.page}
                            />
                        </Col>
                    </Row>
                </Col>
            )}
        </Row>
    );
};

export default TableWrapper;