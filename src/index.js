import React from "react";
import ReactDOM from "react-dom";

// import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";

import App from "./App.jsx";
import reportWebVitals from "./reportWebVitals";

import "antd/dist/reset.css";
// import "./index.css";
import './App.css'
import Store from "./store";

import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();
ReactDOM.render(
  // <React.StrictMode>
  // <Router history={Utils.history}>

  <Router basename="/">
    <QueryClientProvider client={queryClient}>
      <Provider store={Store}>
        <App/>
      </Provider>
    </QueryClientProvider>
  </Router>,
  // </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();

