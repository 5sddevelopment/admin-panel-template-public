import React, { useState } from 'react'
import { Col, Row, Steps } from 'antd';
import Form1 from '../../components/TabsFormComponent/Form1';
import Form3 from '../../components/TabsFormComponent/Form3';
import Form2 from '../../components/TabsFormComponent/Form2';
import { Button } from '../../components';
import { FormProvider, useForm, useFormContext } from 'react-hook-form';
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup"


const schema = yup.object().shape({
    input: yup.string().required(),
    checkBox: yup.string().required(),
    datepickr: yup.string().required(),
    input: yup.string().required(),
    radioGroup: yup.string().required(),
    select: yup.string().required(),
    switch: yup.string().required(),
    textArea: yup.string().required(),

    input2: yup.string().required(),
    checkBox2: yup.string().required(),
    datepickr2: yup.string().required(),
    input2: yup.string().required(),
    radioGroup2: yup.string().required(),
    select2: yup.string().required(),
    switch2: yup.string().required(),
    textArea2: yup.string().required(),

    input3: yup.string().required(),
    checkBox3: yup.string().required(),
    datepickr3: yup.string().required(),
    input3: yup.string().required(),
    radioGroup3: yup.string().required(),
    select3: yup.string().required(),
    switch3: yup.string().required(),
    textArea3: yup.string().required()
});


const TabsForm = () => {

    const [state, setState] = useState(0)

    const methods = useForm({
        mode: 'onChange',
        // defaultValues: {},
        resolver: yupResolver(schema),
    });
    const { reset, watch, control, onChange, formState: { errors, isValid }, getValues } = methods;
    const form = watch();


    const onHandleSubmit = () => {
        console.log(getValues());

    }
    console.log("fianll=>", form, errors);


    return (
        <FormProvider {...methods}>
            <Steps

                style={{ paddingBottom: "25px" }}
                current={state}
                items={[
                    {
                        title: 'start',
                        // description,
                    },
                    {
                        title: 'In Progress',
                        // description,

                    },
                    {
                        title: '3rd step',
                        // description,
                    },
                    {
                        title: 'End',
                        // description,
                    },
                ]}
            />
            <div className={state !== 0 ? 'hideshow' : ''}>
                <Form1 />
            </div>

            <div className={state !== 1 ? 'hideshow' : ''}>
                <Form2 />
            </div>

            <div className={state !== 2 ? 'hideshow' : ''}>
                <Form3 />
            </div>

            <Row gutter={[4, 4]} justify={"end"}>
                <Col>
                    <Button.Basic
                        onClick={() => setState(state - 1)}
                        text={"Back"}
                    />
                </Col>
                <Col>
                    <Button.Basic
                        onClick={() => setState(state + 1)}
                        text={"Next"}
                    />
                </Col>

                <Col>
                    <Button.Basic
                        onClick={() => onHandleSubmit()}
                        text={"Submit"}
                    />
                </Col>

            </Row>
        </FormProvider>
    )
}

export default TabsForm