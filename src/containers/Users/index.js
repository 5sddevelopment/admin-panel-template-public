import React from "react";
import { connect } from "react-redux";
import { Button, TableWrapper } from "../../components";
import styles from "./styles"
import { Utils } from "../../config"
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { Col, Row } from "antd";
const { history } = Utils


const Users = () => {

    const navigate = useNavigate()
    const dispatch = useDispatch()

    return (
        <div style={styles.container}>

            <Row justify={"space-between"} style={{ padding: "5px" }} align={"middle"}>
                <Col>
                    User 100
                </Col>
                <Col>
                    <Button.Basic
                        color={"white"}
                        text={"Delete"}
                        buttonStyle={{ back: "yellow" }}

                    />
                </Col>

            </Row>
            <TableWrapper
                tableData={data}
                tableColumns={columns}
            />
        </div>
    )
}
export default Users

const columns = [
    {
        title: "NAME",
        dataIndex: "name",
        align: "left",
        // sorter: {
        //     compare: (a, b) => a.name - b.name,
        //     multiple: 2,
        // },
    },
    {
        title: "EMAIL ADDRESS",
        dataIndex: "email",
        align: "left",
        // sorter: {
        //     compare: (a, b) => a.email - b.email,
        //     multiple: 3,
        // },
    },
    {
        title: "ACTION",
        dataIndex: "action",
        align: "left",
    },
    {
        title: "ACTION",
        dataIndex: "action",
        align: "left",
    },

    {
        title: "ACTION",
        dataIndex: "action",
        align: "left",

        // render: (text, record) => (
        //     <Row gutter={[8, 8]}>
        //         <Col>
        //             <Switch />
        //         </Col>
        //         <Col>
        //             <Button.Basic
        //                 text="Delete"
        //                 onClick={() => setIsModalOpen(true)}
        //             />
        //         </Col>
        //     </Row>

        // )
    },
];

const data = [
    {
        key: "1",
        name: "John Brown",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "2",
        name: "Jim Green",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "3",
        name: "Joe Black",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "4",
        name: "Jim Red",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "5",
        name: "Jim Red",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "6",
        name: "Jim Red",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
];
