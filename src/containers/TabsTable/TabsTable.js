import React,{useState} from "react";
import { AndroidOutlined, AppleOutlined } from "@ant-design/icons";
import Tabs from "../../components/Tabs";
import { TableWrapper } from "../../components";
import BasicTable from "../BasicTable/BasicTable";
import { Button, Modal } from "../../components";
import "./index.css";

const TabsTable = () => {
    const [isModalOpen, setIsModalOpen] = useState(false)
  const onChangeTab = (key) => {
    if (key == "1") {
      // this.setState({
      //     filter: {
      //         ...this.state.filter,
      //         status: "",
      //     },
      // });
      // getManufacturePlants({ ...this.state.filter, status: "" });
    }
    if (key == "2") {
      // this.setState({
      //     filter: {
      //         ...this.state.filter,
      //         status: "pending",
      //     },
      // });
      // getManufacturePlants({
      //     ...this.state.filter,
      //     status: "pending",
      // });
    }
  };
  return (
    <>
      {/* <Button.Basic text="Click Me" onClick={() => setIsModalOpen(true)}/> */}
      <Tabs
        className=""
        onChangeTab={onChangeTab}
        data={[
          {
            key: "ALL",
            component: () => <BasicTable />,
          },
          {
            key: "PENDING",
            component: () => <BasicTable />,
          },
        ]}
      />
      <Modal.BasicModal
        styleClass="BasicModalStyling"
        modalType="basicModal"
        isModalOpen={isModalOpen}
        onCancel={()=>{
            setIsModalOpen(false)
        }}
        title="Random title"
        text='random text for testing'
        isFooter={{
            showFooter:true,
            leftButtonText:'Left button',
            rightButtonText:'Right button',
            onClickLeft:()=>alert('on left click'),
            onClickRight:()=>alert('on right click'),
        }}
        // isCommentModal={{
        //   commentBoxMaxLength: 5,
        //   commentBoxShowLenth: true,
        //   commentBoxRows:5,
        //   btnSubmitText:'Submit'
        // }}
      />
    </>
  );
};

export default TabsTable;

const columns = [
  {
    title: "NAME",
    dataIndex: "name",
    align: "left",
    // sorter: {
    //     compare: (a, b) => a.name - b.name,
    //     multiple: 2,
    // },
  },
  {
    title: "EMAIL ADDRESS",
    dataIndex: "email",
    align: "left",
    // sorter: {
    //     compare: (a, b) => a.email - b.email,
    //     multiple: 3,
    // },
  },
  {
    title: "ACTION",
    dataIndex: "action",
    align: "left",
  },
  {
    title: "ACTION",
    dataIndex: "action",
    align: "left",
  },

  {
    title: "ACTION",
    dataIndex: "action",
    align: "left",
  },
];

const data2 = [
  {
    key: "1",
    name: "John @2",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "2",
    name: "Jim Green",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "3",
    name: "Joe Black",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "4",
    name: "Jim Red",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "5",
    name: "Jim Red",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "6",
    name: "Jim Red",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
];

const data = [
  {
    key: "1",
    name: "John Brown",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "2",
    name: "Jim Green",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "3",
    name: "Joe Black",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "4",
    name: "Jim Red",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "5",
    name: "Jim Red",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
  {
    key: "6",
    name: "Jim Red",
    email: "zain@yopmail.com",
    action: "ALLOW",
  },
];
