import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Button } from "../../components";
import { AppAction } from "../../store/actions";
import styles from "./styles"
import { Row, Col } from "antd";
import { useNavigate } from "react-router-dom";
import { message } from 'antd';
import { Utils } from "../../config";
import BasicCard from "../../components/Card/BasicCard";
// import LineChart from "../../components/Charts/LineChart";
import {
    Charts,
} from "../../components";
import BarCharJs from "../../components/Charts/BarChart";

let inititialState = {
    toggle: "",
    date: "",
    time: "",
    open: false,
    snackBar: false,
    paginationCount: 5,
    paginationOffset: 0,
}

const Dashboard = () => {

    const navigate = useNavigate()
    const dispatch = useDispatch()

    const [state, setState] = useState(inititialState)


    return (
        <div className="container">
            <Row gutter={[16, 16]} >


                <Col xs={24} lg={8}>
                    <BasicCard content="Card 1" />
                </Col>
                <Col xs={24} lg={8}>
                    <BasicCard content="Card 2" />
                </Col>
                <Col xs={24} lg={8}>
                    <BasicCard content="Card 3" />
                </Col>



                <br />

                {/* <Row gutter={[16, 16]} > */}
                <Col className="gutter-row" xs={24} lg={12}>
                    <div style={{ ...styles.shadowViewContainer, }}>
                        <Charts.ResponsiveContainer
                            style={{ margin: "27px 0px" }}
                            height={350}
                        >
                            {[
                                {
                                    MinutesWithVoiceChat: 3000,
                                    name: "Aug",
                                    MinutesWithoutVoiceChat: 1000,
                                },
                                {
                                    MinutesWithVoiceChat: 2500,
                                    name: "Sept",
                                    MinutesWithoutVoiceChat: 1500,
                                },
                                {
                                    MinutesWithVoiceChat: 2000,
                                    name: "Oct",
                                    MinutesWithoutVoiceChat: 2500,
                                },
                                {
                                    MinutesWithVoiceChat: 1500,
                                    name: "Nov",
                                    MinutesWithoutVoiceChat: 3000,
                                },
                            ]
                                .map((val) => val.MinutesWithoutVoiceChat)
                                .reduce((total, value) => total + value, 0) > 0 ? (
                                <Charts.LineChart
                                    data={[
                                        {
                                            MinutesWithVoiceChat: 3000,
                                            name: "Aug",
                                            MinutesWithoutVoiceChat: 1000,
                                        },
                                        {
                                            MinutesWithVoiceChat: 2500,
                                            name: "Sept",
                                            MinutesWithoutVoiceChat: 1500,
                                        },
                                        {
                                            MinutesWithVoiceChat: 2000,
                                            name: "Oct",
                                            MinutesWithoutVoiceChat: 2500,
                                        },
                                        {
                                            MinutesWithVoiceChat: 1500,
                                            name: "Nov",
                                            MinutesWithoutVoiceChat: 3000,
                                        },
                                    ]}
                                    lineColor={["#4BE5C0", "#4E5FF8"]}
                                    loading={state.appDownloadsLoading}
                                    margin={{ top: 10, right: 50, left: 0, bottom: 0 }}
                                />
                            ) : (
                                <h4
                                    style={{
                                        position: "absolute",
                                        top: "50%",
                                        left: "50%",
                                        transform: "translate(-50%, -50%)",
                                    }}
                                >
                                    No Data Available for month range!
                                </h4>
                            )}
                        </Charts.ResponsiveContainer>
                    </div>
                </Col>
                <Col className="gutter-row" xs={24} lg={12}>
                    <div style={{ ...styles.shadowViewContainer }}>
                        <Charts.ResponsiveContainer
                            // style={{ margin: "27px 0px" }}
                            height={400}
                            width={"100%"}
                        >
                            {[
                                { name: "Group A", price: 400 },
                                { name: "Group B", price: 300 },
                                { name: "Group C", price: 300 },
                            ]
                                .map((val) => val.price)
                                .reduce((total, value) => total + value, 0) > 0 ? (
                                <Charts.PieChart
                                    data={[
                                        { name: "Group A", price: 10 },
                                        { name: "Group B", price: 300 },
                                        { name: "Group C", price: 300 },
                                    ]}
                                    lineColor={["#4BE5C0", "#4E5FF8"]}
                                    loading={state.appDownloadsLoading}
                                // margin={{ top: 10, right: 50, left: 0, bottom: 0 }}
                                />
                            ) : (
                                <h4
                                    style={{
                                        position: "absolute",
                                        top: "50%",
                                        left: "50%",
                                        transform: "translate(-50%, -50%)",
                                    }}
                                >
                                    No Data Available for month range!
                                </h4>
                            )}
                        </Charts.ResponsiveContainer>
                    </div>
                </Col>


                <Col className="gutter-row" xs={24} lg={12}>
                    <div style={{ ...styles.shadowViewContainer }}>

                        <BarCharJs />
                    </div>
                </Col>




            </Row>
        </div>
    )
}
export default Dashboard;