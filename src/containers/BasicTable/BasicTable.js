import React, { useState } from 'react'
import { Button, TableWrapper,Modal } from '../../components'
import { Row, Col, Switch } from 'antd'
import styles from './style';
import '../../App.css'

const data = [
    {
        key: "1",
        name: "John Brown",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "2",
        name: "Jim Green",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "3",
        name: "Joe Black",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "4",
        name: "Jim Red",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "5",
        name: "Jim Red",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
    {
        key: "6",
        name: "Jim Red",
        email: "zain@yopmail.com",
        action: "ALLOW",
    },
];

const BasicTable = () => {
    const [isModalOpen, setIsModalOpen] = useState(false)

    const columns = [
        {
            title: "NAME",
            dataIndex: "name",
            align: "left",
            // sorter: {
            //     compare: (a, b) => a.name - b.name,
            //     multiple: 2,
            // },
        },
        {
            title: "EMAIL ADDRESS",
            dataIndex: "email",
            align: "left",
            // sorter: {
            //     compare: (a, b) => a.email - b.email,
            //     multiple: 3,
            // },
        },
        {
            title: "ACTION",
            dataIndex: "action",
            align: "left",
        },
        {
            title: "ACTION",
            dataIndex: "action",
            align: "left",
        },

        {
            title: "ACTION",
            dataIndex: "action",
            align: "left",

            render: (text, record) => (
                <Row gutter={[8, 8]}>
                    <Col>
                        <Switch />
                    </Col>
                    <Col>
                        <Button.Basic
                            text="Delete"
                            onClick={() => setIsModalOpen(true)}
                        />
                    </Col>
                </Row>

            )
        },
    ];

    return (
        <div className='container shadowViewContainer'>
            <TableWrapper tableColumns={columns} tableData={data} />



            <Modal.BasicModal
                styleClass="BasicModalStyling"
                modalType="basicModal"
                isModalOpen={isModalOpen}
                onCancel={() => {
                    setIsModalOpen(false)
                }}
                title="Random title"
                text='random text for testing'
                isFooter={{
                    showFooter: true,
                    leftButtonText: 'Left button',
                    rightButtonText: 'Right button',
                    onClickLeft: () => alert('on left click'),
                    onClickRight: () => alert('on right click'),
                }}
            // isCommentModal={{
            //   commentBoxMaxLength: 5,
            //   commentBoxShowLenth: true,
            //   commentBoxRows:5,
            //   btnSubmitText:'Submit'
            // }}
            />
        </div>
    )
}

export default BasicTable