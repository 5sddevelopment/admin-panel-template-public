import { Colors } from "../../config";

const styles = {
    container: {
        // height:"100vh",
        display: "flex",
        flexFlow: "column wrap",
        backgroundColor: "rgb(240, 242, 245)",
        justifyContent: 'center',
        paddingLeft: "15px",
        paddingRight: "18px",
        paddingTop: "15px"
    },
    shadowViewContainer: {
        display: "flex",
        backgroundColor: Colors.White,
        justifyContent: 'center',
        alignItems: 'center',
        // margin: "20px",
        // padding: "20px",
        boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
    },
}

export default styles