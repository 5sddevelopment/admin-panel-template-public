import React from 'react'
import { Button, Picker, Select, Switch, Text, TextField } from '../../components'
import { Row, Col, Form } from 'antd'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup"
import CheckBoxField from '../../components/Select/Checkbox';


const schema = yup.object().shape({
    input: yup.string().required(),
    checkBox: yup.string().required(),
    datepickr: yup.string().required(),
    input: yup.string().required(),
    radioGroup: yup.string().required(),
    select: yup.string().required(),
    switch: yup.string().required(),
    textArea: yup.string().required()
});

let defaultValue = {
    checkBox: "",
    datepickr: "2023-05-01",
    input: "",
    radioGroup: "",
    select: "",
    switch: "",
    textArea: ""

}


const FormBasic = () => {

    const { register, handleSubmit, formState: { errors, isValid }, reset, setValue, control }
        = useForm({ mode: "onChange", resolver: yupResolver(schema), defaultValues: defaultValue });


    function onChangeDate(date, dateString) {
        setValue("datepickr", dateString)
    }
    const onSubmitHandler = (data) => {

        console.log("dfdfsdfd", data);
        // reset()
    };
    console.log(errors, isValid);

    return (
        <>
            <form onSubmit={handleSubmit(onSubmitHandler)} className='container' style={{
                paddingLeft: "25%",
                paddingRight: "25%"
            }} >
                <Row gutter={[16, 10]}  >

                    <Col xs={24}>
                        <span style={{ color: "black", fontWeight: "bold" }}>CheckBox</span>
                    </Col>

                    <Col xs={24}>
                        <Controller
                            name="checkBox"
                            type="checkbox"
                            control={control}
                            render={({ field: { onChange, value, onBlur, ref } }) => (
                                <CheckBoxField
                                    label="price"
                                    checked={value}
                                    onChange={(ev) => onChange(ev.target.checked)}

                                />

                            )}
                        />
                        <span style={{ color: "red" }}>{errors?.checkBox?.message}</span>
                    </Col>

                    <Col xs={24}>
                        <span style={{ color: "black", fontWeight: "bold" }}>Gender</span>
                    </Col>
                    <Col xs={24}>
                        <Controller
                            name="radioGroup"
                            control={control}
                            render={({ field: { onChange, value, onBlur, ref } }) => (
                                <Select.Radio
                                    label={"Gender"}
                                    options={["Male", "Female"]}
                                    onChange={(e) => onChange(e.target.value)}
                                />
                            )}

                        />

                        <span style={{ color: "red" }}>{errors?.radioGroup?.message}</span>
                    </Col>
                    <Col xs={24}>
                        <span style={{ color: "black", fontWeight: "bold" }}>Input</span>
                    </Col>

                    <Col xs={24}>
                        <Controller
                            name="input"
                            control={control}
                            render={({ field: { onChange, value, onBlur, ref } }) => (
                                <TextField.Basic
                                    // label={"Input"}
                                    onChange={(e) => onChange(e.target.value)}
                                />
                            )}
                        />
                        <span style={{ color: "red" }}>  {errors?.input?.message}</span>
                    </Col>

                    <Col xs={24}>
                        <span style={{ color: "black", fontWeight: "bold" }}>Select</span>
                    </Col>
                    <Col xs={24}>
                        <Controller
                            name="select"
                            control={control}
                            render={({ field: { onChange, value, onBlur, ref } }) => (
                                <TextField.SelectField
                                    value={value}
                                    size={"large"}
                                    style={{ width: "150px" }}
                                    defaultValue="lucy"
                                    placeholder={"select"}
                                    options={[
                                        { value: 'jack', label: 'Jack' },
                                        { value: 'lucy', label: 'Lucy' },
                                        { value: 'Yiminghe', label: 'yiminghe' },

                                    ]}
                                    onChange={onChange}

                                />


                            )}
                        />
                        <span style={{ color: "red" }}>  {errors?.select?.message}</span>
                    </Col>

                    <Col xs={24}>
                        <span style={{ color: "black", fontWeight: "bold" }}>DatePicker</span>
                    </Col>
                    <Col xs={24}>
                        <Picker.DatePicker
                            onChange={onChangeDate}
                            format={'YYYY-MM-DD'}
                        />
                        <span style={{ color: "red" }}>{errors?.datepickr?.message}</span>
                    </Col>
                    <Col xs={24}>
                        <span style={{ color: "black", fontWeight: "bold" }}>Switch</span>
                    </Col>

                    <Col xs={24}>
                        <Controller
                            name="switch"
                            control={control}
                            render={({ field: { onChange, value, onBlur, ref } }) => (
                                <Switch.Basic
                                    checked={value}
                                    onBlur={onBlur}
                                    onChange={onChange}
                                    size={"large"}
                                // label="Switch"
                                />
                            )}
                        />
                        <span style={{ color: "red" }}>{errors?.switch?.message}</span>
                    </Col>
                    <Col xs={24}>
                        <span style={{ color: "black", fontWeight: "bold" }}>TextArea</span>
                    </Col>
                    <Col xs={24}>
                        <Controller
                            name="textArea"
                            control={control}
                            render={({ field: { onChange, value, onBlur, ref } }) => (
                                <TextField.Textarea
                                    value={value}
                                    onChange={(e) => onChange(e.target.value)}
                                    placeholder={"Text Area"}
                                />
                            )}
                        />
                        <span style={{ color: "red" }}> {errors?.textArea?.message}</span>
                    </Col>

                    <Col xs={24}>
                        <Button.Basic text="Submit" type="primary" htmlType={"submit"} />
                    </Col>
                </Row>

            </form>
        </>
    )
}

export default FormBasic